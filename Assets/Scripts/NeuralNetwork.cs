using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetwork : MonoBehaviour
{
    public List<Layer> layers = new List<Layer>();
    [Range(-1f, 1f)] public float in1, in2, in3, in4;


    void Start()
    {
        Layer inputLayer = new Layer();
        Layer hiddenLayer1 = new Layer();
        Layer hiddenLayer2 = new Layer();
        Layer hiddenLayer3 = new Layer();
        Layer hiddenLayer4 = new Layer();
        Layer outputLayer = new Layer();

        layers = new List<Layer>() {
            inputLayer,
            hiddenLayer1,
            hiddenLayer2,
            hiddenLayer3,
            outputLayer
        };

        Neuron inputNeuron1 = new Neuron(0, 0);
        inputNeuron1.Value = -1f;
        Neuron inputNeuron2 = new Neuron(0, 2);
        inputNeuron2.Value = -1f;
        Neuron inputNeuron3 = new Neuron(0, 4);
        inputNeuron3.Value = 1f;
        Neuron inputNeuron4 = new Neuron(0, 6);
        inputNeuron4.Value = 1f;

        Neuron processingNeuron1 = new Neuron(1, 0);
        Neuron processingNeuron2 = new Neuron(1, 2);
        Neuron processingNeuron3 = new Neuron(1, 4);
        Neuron processingNeuron4 = new Neuron(1, 6);

        Neuron processingNeuron5 = new Neuron(2, 0);
        Neuron processingNeuron6 = new Neuron(2, 2);
        Neuron processingNeuron7 = new Neuron(2, 4);
        Neuron processingNeuron8 = new Neuron(2, 6);

        Neuron processingNeuron9 = new Neuron(3, 0, true);
        Neuron processingNeuron10 = new Neuron(3, 1, true);
        Neuron processingNeuron11 = new Neuron(3, 2, true);
        Neuron processingNeuron12 = new Neuron(3, 3, true);
        Neuron processingNeuron13 = new Neuron(3, 4, true);
        Neuron processingNeuron14 = new Neuron(3, 5, true);
        Neuron processingNeuron15 = new Neuron(3, 6, true);
        Neuron processingNeuron16 = new Neuron(3, 7, true);

        Neuron outputNeuron1 = new Neuron(4, 0);
        Neuron outputNeuron2 = new Neuron(4, 2);
        Neuron outputNeuron3 = new Neuron(4, 4);
        Neuron outputNeuron4 = new Neuron(4, 6);

        new Connector(inputNeuron1, processingNeuron1, 1.0f);
        new Connector(inputNeuron1, processingNeuron3, 1.0f);
        new Connector(inputNeuron2, processingNeuron2, 1.0f);
        new Connector(inputNeuron2, processingNeuron4, 1.0f);
        new Connector(inputNeuron3, processingNeuron2, 1.0f);
        new Connector(inputNeuron3, processingNeuron4, -1.0f);
        new Connector(inputNeuron4, processingNeuron1, 1.0f);
        new Connector(inputNeuron4, processingNeuron3, -1.0f);


        new Connector(processingNeuron1, processingNeuron5, 1.0f);
        new Connector(processingNeuron1, processingNeuron6, -1.0f);
        new Connector(processingNeuron2, processingNeuron5, 1.0f);
        new Connector(processingNeuron2, processingNeuron6, 1.0f);

        new Connector(processingNeuron3, processingNeuron7, 1.0f);
        new Connector(processingNeuron3, processingNeuron8, 1.0f);
        new Connector(processingNeuron4, processingNeuron7, -1.0f);
        new Connector(processingNeuron4, processingNeuron8, 1.0f);


        new Connector(processingNeuron5, processingNeuron9, 1.0f);
        new Connector(processingNeuron5, processingNeuron10, -1.0f);
        new Connector(processingNeuron6, processingNeuron11, 1.0f);
        new Connector(processingNeuron6, processingNeuron12, -1.0f);
        new Connector(processingNeuron7, processingNeuron13, 1.0f);
        new Connector(processingNeuron7, processingNeuron14, -1.0f);
        new Connector(processingNeuron8, processingNeuron15, 1.0f);
        new Connector(processingNeuron8, processingNeuron16, -1.0f);

        new Connector(processingNeuron9, outputNeuron1, 1.0f);
        new Connector(processingNeuron10, outputNeuron1, 1.0f);
        new Connector(processingNeuron11, outputNeuron2, 1.0f);
        new Connector(processingNeuron12, outputNeuron2, 1.0f);
        new Connector(processingNeuron13, outputNeuron3, 1.0f);
        new Connector(processingNeuron14, outputNeuron3, 1.0f);
        new Connector(processingNeuron15, outputNeuron4, 1.0f);
        new Connector(processingNeuron16, outputNeuron4, 1.0f);

        inputLayer.neurons.AddRange(new Neuron[] {
            inputNeuron1,
            inputNeuron2,
            inputNeuron3,
            inputNeuron4
        });

        hiddenLayer1.neurons.AddRange(new Neuron[] {
            processingNeuron1,
            processingNeuron2,
            processingNeuron3,
            processingNeuron4
        });

        hiddenLayer2.neurons.AddRange(new Neuron[] {
            processingNeuron5,
            processingNeuron6,
            processingNeuron7,
            processingNeuron8
        });

        hiddenLayer3.neurons.AddRange(new Neuron[] {
            processingNeuron9,
            processingNeuron10,
            processingNeuron11,
            processingNeuron12,
            processingNeuron13,
            processingNeuron14,
            processingNeuron15,
            processingNeuron16
        });

        outputLayer.neurons.AddRange(new Neuron[] {
            outputNeuron1,
            outputNeuron2,
            outputNeuron3,
            outputNeuron4
        });

    }

    void Update()
    {
        List<Neuron> neurons = layers[0].neurons;
        neurons[0].Value = in1;
        neurons[1].Value = in2;
        neurons[2].Value = in3;
        neurons[3].Value = in4;


        UpdateLayers();
    }

    public void UpdateLayers()
    {
        for (int layerIndex = 1; layerIndex < layers.Count; layerIndex++)
        {
            Layer layer = layers[layerIndex];

            foreach (Neuron neuron in layer.neurons)
            {
                neuron.Value = 0.0f;
            }

            UpdateLayer(layer);
        }
    }

    public void UpdateLayer(Layer layer)
    {
        foreach (Neuron neuron in layer.neurons)
        {

            float sum = 0.0f;

            foreach (Connector connector in neuron.connections)
            {
                if (connector.endNeuron == neuron)
                {
                    sum += connector.weight * connector.startNeuron.Value;
                }
            }

            neuron.Value = sum;
        }
    }
}

public class Layer
{
    public List<Neuron> neurons = new List<Neuron>();
}

public class Neuron
{
    private float value;

    public NeuronRenderer renderer;

    private bool useReLU = false;

    public float Value
    {
        get
        {
            return Mathf.Clamp(value, useReLU ? 0f : -1f, 1f);
        }

        set
        {
            this.value = Mathf.Clamp(value, useReLU ? 0f : -1f, 1f);

            renderer.value = value;
        }
    }

    public Neuron(int layerIndex, int heightIndex, bool useReLU = false)
    {
        // Getting World Position For Neuron
        float xPos = (-8 + layerIndex * 3);
        float yPos = (3 - heightIndex);
        Vector2 position = new Vector2(xPos, yPos);

        // Instantiate Neuron Renderer
        GameObject neuronObject = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Neuron"), position, Quaternion.identity);
        renderer = neuronObject.GetComponent<NeuronRenderer>();

        this.useReLU = useReLU;
    }

    public List<Connector> connections = new List<Connector>();
}

public class Connector
{
    public float weight = 0.0f;

    private LineRenderer renderer;

    public Neuron startNeuron;

    public Neuron endNeuron;

    public Connector(Neuron startNeuron, Neuron endNeuron, float weight)
    {
        // Initalise Variables
        this.startNeuron = startNeuron;

        this.endNeuron = endNeuron;

        this.weight = weight;

        startNeuron.connections.Add(this);
        endNeuron.connections.Add(this);

        // Instantiate Connector Renderer
        GameObject connectorObject = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Connector"), Vector3.zero, Quaternion.identity);
        renderer = connectorObject.GetComponent<LineRenderer>();

        // Initalise Renderer Variables
        renderer.SetPosition(0, startNeuron.renderer.gameObject.transform.position);
        renderer.SetPosition(1, endNeuron.renderer.gameObject.transform.position);

        float connectorColor = (weight + 1f) / 2f;
        renderer.startColor = new Color(connectorColor, connectorColor, connectorColor);
        renderer.endColor = new Color(connectorColor, connectorColor, connectorColor);


    }
}