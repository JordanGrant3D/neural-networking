using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NeuronRenderer : MonoBehaviour
{
    [HideInInspector] public float value;

    private SpriteRenderer valueDisplayer;

    public TMP_Text valueText;

    public void Update()
    {
        Render();
    }

    public void Render()
    {
        valueText.text = value.ToString("0.0");
        if (valueDisplayer == null)
        {
            valueDisplayer = transform.Find("Value").GetComponent<SpriteRenderer>();
        }

        float valueColor = (value + 1f) / 2f;

        valueDisplayer.color = new Color(valueColor, valueColor, valueColor);
    }
}
